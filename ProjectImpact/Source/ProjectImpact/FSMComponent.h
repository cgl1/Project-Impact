// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FSMComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnEnterState, uint8, prevState, uint8, currState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTickState, uint8, currState, float, deltaTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnExitState, uint8, currState, uint8, nextState);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UFSMComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "FSM")
	FOnEnterState OnEnterState;

	UPROPERTY(BlueprintAssignable, Category = "FSM")
	FOnTickState OnTickState;

	UPROPERTY(BlueprintAssignable, Category = "FSM")
	FOnEnterState OnExitState;
	
	UPROPERTY(BlueprintReadWrite, Category = "FSM")
	bool bPrintErrorsToLog;
		
private:
	int32 m_NumOfStates = 0;
	uint8 m_CurrState = 0;
	float m_CurrStateBeginTime = 0;
	bool m_IsInitialized;

public:
	// Sets default values for this component's properties
	UFSMComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, Category="FSM")
	virtual bool InitFSM(const int32 numberOfStates, const uint8 initialState);

	UFUNCTION(BlueprintCallable, Category = "FSM")
	virtual bool SetState(const uint8 state);

	UFUNCTION(BlueprintPure, Category = "FSM")
	bool IsValidState(const uint8 state) const;

	UFUNCTION(BlueprintPure, Category = "FSM")
	uint8 GetCurrentState() const;

	UFUNCTION(BlueprintPure, Category = "FSM")
	float GetTime() const;
};
