// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableComponentBase.h"

#include "InteractorComponent.h"

// Sets default values for this component's properties
UInteractableComponentBase::UInteractableComponentBase()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractableComponentBase::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UInteractableComponentBase::SetIsInteractable(bool value)
{
	bool prevIsInteractable = m_IsInteractable;
	m_IsInteractable = value;
	for (UInteractorComponent* interactor : m_RegisteredInteractors)
	{
		interactor->RefreshInteractableCandidate();
	}

	if (value != prevIsInteractable)
	{
		OnIsInteractableChanged.Broadcast(value);
	}
}

FInteractResult UInteractableComponentBase::Interact(UInteractorComponent* interactor)
{
	FInteractResult result = handleOnInteract(interactor);
	OnInteracted.Broadcast(interactor, result);
	return result;
}

void UInteractableComponentBase::OnBecomeBestCandidateOf(UInteractorComponent* interactor)
{
	m_BestCandidatedInteractors.AddUnique(interactor);
	handleOnBecomeBestCandidateOf(interactor);
	OnBecomeBestCandidate.Broadcast(interactor);
}

void UInteractableComponentBase::OnDisqualifiedAsBestCandidateOf(UInteractorComponent* interactor)
{
	m_BestCandidatedInteractors.Remove(interactor);
	handleOnDisqualifiedAsBestCandidateOf(interactor);
	OnDisqualifiedAsBestCandidate.Broadcast(interactor);
}

void UInteractableComponentBase::OnRegisteredTo(UInteractorComponent* interactor)
{
	m_RegisteredInteractors.AddUnique(interactor);
	handleOnRegisteredTo(interactor);
}

void UInteractableComponentBase::OnUnregisteredFrom(UInteractorComponent* interactor)
{
	m_RegisteredInteractors.Remove(interactor);
	handleOnUnregisteredFrom(interactor);
}

void UInteractableComponentBase::handleOnRegisteredTo_Implementation(UInteractorComponent* interactor)
{
}

void UInteractableComponentBase::handleOnUnregisteredFrom_Implementation(UInteractorComponent* interactor)
{
}

FInteractResult UInteractableComponentBase::handleOnInteract_Implementation(UInteractorComponent* interactor)
{
	return FInteractResult{};
}

void UInteractableComponentBase::handleOnBecomeBestCandidateOf_Implementation(UInteractorComponent* interactor)
{
}

void UInteractableComponentBase::handleOnDisqualifiedAsBestCandidateOf_Implementation(UInteractorComponent* interactor)
{
}

