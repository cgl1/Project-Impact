// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ExerciseData.h"
#include "ManifestationActorBase.h"
#include "IncantationData.generated.h"

USTRUCT(BlueprintType)
struct FExerciseSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UExerciseData* Exercise;
};

/**
 * 
 */
UCLASS(BlueprintType)
class PROJECTIMPACT_API UIncantationData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString IncantationName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AManifestationActorBase> ManifestationType;

	/** The sequence of exercises to cast the incantation */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = Exercise))
	TArray<FExerciseSettings> ExerciseList;
};
