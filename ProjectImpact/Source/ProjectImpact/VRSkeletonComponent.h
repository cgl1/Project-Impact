// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MotionControllerComponent.h"
#include "VRSkeletonComponent.generated.h"

USTRUCT(BlueprintType)
struct FVRSkeletonData
{
	GENERATED_BODY()
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector HeadPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector HeadForwardVector;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator HeadOrientation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector HeadUpVector = FVector{0, 0, 1};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector NeckPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector LeftHandPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector RightHandPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector LeftShoulderPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector RightShoulderPosition;
};

USTRUCT(BlueprintType)
struct FPoseCheckParameters
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftArmLength = 60;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightArmLength = 60;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ArmLengthTolerance = 10;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector LeftArmDirection = FVector{0, 0, -1};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector RightArmDirection = FVector{0, 0, -1};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ArmAngleToleranceInDegree = 10;
};

USTRUCT(BlueprintType)
struct FPoseCheckResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool LeftSuccess = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool RightSuccess = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftArmLength = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightArmLength = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float LeftArmAngleDiffInDegree = 15;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RightArmAngleDiffInDegree = 15;
};

USTRUCT(BlueprintType)
struct FPoseHandLocations
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector LeftHand = FVector{0, 0, -1};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector RightHand = FVector{0, 0, -1};
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UVRSkeletonComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HeadToNeckOffsetZ = -20;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ShoulderWidth = 40;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ArmFullLength = 60;

private:
	UMotionControllerComponent* m_LeftMotionController = nullptr;
	UMotionControllerComponent* m_RightMotionController = nullptr;

public:	
	// Sets default values for this component's properties
	UVRSkeletonComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	void SetMotionControllers(UMotionControllerComponent* leftController, UMotionControllerComponent* rightController);

	UFUNCTION(BlueprintCallable)
	FVector GetHeadWorldLocation();

	UFUNCTION(BlueprintCallable)
	FVector GetHeadForwardVector();
	
	UFUNCTION(BlueprintCallable)
	FVector GetNeckWorldLocation();
	
	UFUNCTION(BlueprintCallable)
	FVector GetLeftHandLocation();

	UFUNCTION(BlueprintCallable)
	FVector GetRightHandLocation();

	UFUNCTION(BlueprintCallable)
	FVector GetLeftShoulderLocation();

	UFUNCTION(BlueprintCallable)
	FVector GetRightShoulderLocation();

	UFUNCTION(BlueprintCallable)
	FVRSkeletonData GetSkeletonData();

	UFUNCTION(BlueprintCallable)
	FPoseCheckResult CheckPose(FPoseCheckParameters parameters);

	UFUNCTION(BlueprintCallable)
	FPoseHandLocations GetPoseHandLocations(FPoseCheckParameters parameters);

	UFUNCTION(BlueprintCallable)
	void DrawDebugSkeleton(FVector skeletonOffset);

	UFUNCTION(BlueprintCallable)
	void DrawDebugPose(FPoseCheckParameters parameters, FVector skeletonOffset);
};
