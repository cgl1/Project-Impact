// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "InteractorComponent.generated.h"

class UInteractableComponentBase;

USTRUCT(BlueprintType)
struct FTriggerInteractResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

USTRUCT(BlueprintType)
struct FRegisterInteractableResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

USTRUCT(BlueprintType)
struct FUnregisterInteractableResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnBestCandidateChangedEvent, UInteractableComponentBase*, prevBestCandidate, UInteractableComponentBase*, newBestCandidate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UInteractorComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bHasCandidateDistanceThreshold = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCandidateDistanceThreshold"))
	float CandidateDistanceThreshold = 50.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bRefreshBestCandidateOnTick = false;

	UPROPERTY(BlueprintAssignable)
	FOnBestCandidateChangedEvent OnBestCandidateChanged;

private:
	TArray<UInteractableComponentBase*> m_InteractableCandidates;
	UInteractableComponentBase* m_BestCandidate = nullptr;

	TArray<UInteractableComponentBase*> m_PreallocatedInteractablesArray;

public:	
	// Sets default values for this component's properties
	UInteractorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override; 

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable)
	FTriggerInteractResult InteractWithBestCandidate();

	/* Update best candidate based on the distance and the list of candidates */
	UFUNCTION(BlueprintCallable)
	void RefreshInteractableCandidate();

	UFUNCTION(BlueprintCallable)
	FRegisterInteractableResult RegisterInteractable(UInteractableComponentBase* interactable);

	UFUNCTION(BlueprintCallable)
	FUnregisterInteractableResult UnregisterInteractable(UInteractableComponentBase* interactable);

protected:
	UFUNCTION(BlueprintCallable)
	void handleOnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor);
	
	UFUNCTION(BlueprintCallable)
	void handleOnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor);
};
