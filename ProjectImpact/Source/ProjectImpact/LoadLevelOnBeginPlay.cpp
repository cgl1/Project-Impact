// Fill out your copyright notice in the Description page of Project Settings.


#include "LoadLevelOnBeginPlay.h"

#include "LevelStreamingSubSystem.h"

#if WITH_EDITOR
	#include "Editor/ContentBrowser/Public/IContentBrowserSingleton.h"
	#include "Editor/ContentBrowser/Public/ContentBrowserModule.h"
#endif

// Called when the game starts or when spawned
void ALoadLevelOnBeginPlay::BeginPlay()
{
	Super::BeginPlay();

	ULevelStreamingSubSystem* levelStreamingSystem = GetWorld()->GetSubsystem<ULevelStreamingSubSystem>();

	if (LevelAsset.IsNull())
	{
		// Level asset is invalid.
		UE_LOG(LogTemp, Error, TEXT("Level Asset is null (pointing to an empty object). LoadLevel will not be executed."));
		return;
	}
	levelStreamingSystem->TryUnloadAllLevels();
	levelStreamingSystem->TryLoadLevelAsRoot(LevelAsset.GetLongPackageName());
}

#if WITH_EDITOR

void ALoadLevelOnBeginPlay::smartReferenceLevelPackageName()
{
	// This function is obsolete, but I just want to keep this here for reasearch purpose
	TArray<FAssetData> assetDatas;
	FContentBrowserModule& ContentBrowserModule = FModuleManager::LoadModuleChecked<FContentBrowserModule>("ContentBrowser");
	IContentBrowserSingleton& ContentBrowserSingleton = ContentBrowserModule.Get();
	ContentBrowserSingleton.GetSelectedAssets(assetDatas);

	for (const FAssetData& assetData : assetDatas)
	{
		UE_LOG(LogTemp, Display, TEXT("SELECTED ASSET: %s"), *assetData.PackageName.ToString());
	}

	UE_LOG(LogTemp, Display, TEXT("LEVEL ASSET: %s"), *LevelAsset.GetLongPackageName());
}

#endif // WITH EDITOR


