// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelTransitionSubsystem.h"
#include "LevelTransitionActorBase.generated.h"

UCLASS(Abstract)
class PROJECTIMPACT_API ALevelTransitionActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelTransitionActorBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintImplementableEvent)
	void OnTransitionStateChanged(ETransitionState prevState, ETransitionState newState);

	UFUNCTION(BlueprintImplementableEvent)
	void OnIdleTick(float deltaTime);

	UFUNCTION(BlueprintImplementableEvent)
	void OnStartingTransitionTick(float deltaTime, float progress);

	UFUNCTION(BlueprintImplementableEvent)
	void OnUnloadingTick(float deltaTime);

	UFUNCTION(BlueprintImplementableEvent)
	void OnLoadingTick(float deltaTime);

	UFUNCTION(BlueprintImplementableEvent)
	void OnWaitingTick(float deltaTime, float progress);

	UFUNCTION(BlueprintImplementableEvent)
	void OnEndingTransitionTick(float deltaTime, float progress);
};
