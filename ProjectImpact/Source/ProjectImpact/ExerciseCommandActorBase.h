// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ExerciseCommandActorBase.generated.h"

USTRUCT(BlueprintType)
struct FCommandEvaluationResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

/* The context when the evaluation happens. (i.e. World, DeltaTime) */
USTRUCT(BlueprintType)
struct FEvaluationContext
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* OwnerActor = nullptr;
};

UCLASS()
class PROJECTIMPACT_API AExerciseCommandActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExerciseCommandActorBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void StartCommand();
	void StartCommand_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FCommandEvaluationResult Evaluate(FEvaluationContext context);
	FCommandEvaluationResult Evaluate_Implementation(FEvaluationContext context);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndCommand();
	void EndCommand_Implementation();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void ForceEndCommand();
	void ForceEndCommand_Implementation();
};
