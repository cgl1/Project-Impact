// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectImpact.h"
#include "Modules/ModuleManager.h"

void FProjectImpactModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	//UObject* bpAsset = LoadObject<UObject>(NULL, TEXT("/Game/Blueprints/LevelStreaming/BP_LevelTransitionSubSystem.BP_LevelTransitionSubSystem_C"));
}

void FProjectImpactModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

IMPLEMENT_PRIMARY_GAME_MODULE(FProjectImpactModule, ProjectImpact, "ProjectImpact");
