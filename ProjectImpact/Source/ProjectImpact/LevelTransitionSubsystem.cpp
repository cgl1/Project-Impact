// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTransitionSubsystem.h"

#include "LevelStreamingSubSystem.h"
#include "LevelTransitionActorBase.h"

ETickableTickType ULevelTransitionSubsystem::GetTickableTickType() const
{
	// The CDO of this should never tick
	return IsTemplate() ? ETickableTickType::Never : ETickableTickType::Always;
}

void ULevelTransitionSubsystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ULevelStreamingSubSystem* levelStreamingSystem = tryGetLevelStreamingSubSystem();
	float progress = 0.0f;

	switch (m_State)
	{
	case ETransitionState::Idle:
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnIdleTick(DeltaTime);
			}
		}
		break;
	case ETransitionState::StartingTransition:
		m_StateTimer += DeltaTime;

		progress = FMath::Clamp(m_StateTimer / TransitionSettings.StartingDuration, 0.0f, 1.0f);
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnStartingTransitionTick(DeltaTime, progress);
			}
		}

		if (m_StateTimer >= TransitionSettings.StartingDuration)
		{
			// The state timer is up, start unloading
			levelStreamingSystem->OnUnloadLevelComplete.AddUniqueDynamic(this, &ULevelTransitionSubsystem::handleOnUnloadLevelComplete);
			levelStreamingSystem->TryUnloadAllLevels();
			changeState(ETransitionState::Unloading);
			m_StateTimer = 0.0f;
		}

		break;
	case ETransitionState::Unloading:
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnUnloadingTick(DeltaTime);
			}
		}
		break;
	case ETransitionState::Loading:
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnLoadingTick(DeltaTime);
			}
		}
		break;
	case ETransitionState::Waiting:
		m_StateTimer += DeltaTime;

		progress = FMath::Clamp(m_StateTimer / TransitionSettings.WaitingDuration, 0.0f, 1.0f);
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnWaitingTick(DeltaTime, progress);
			}
		}

		if (m_StateTimer >= TransitionSettings.WaitingDuration)
		{
			changeState(ETransitionState::EndingTransition);
			m_StateTimer -= TransitionSettings.WaitingDuration;
		}

		break;
	case ETransitionState::EndingTransition:
		m_StateTimer += DeltaTime;

		progress = FMath::Clamp(m_StateTimer / TransitionSettings.EndingDuration, 0.0f, 1.0f);
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			// Tick transition actor
			if (actor)
			{
				actor->OnEndingTransitionTick(DeltaTime, progress);
			}
		}

		if (m_StateTimer >= TransitionSettings.EndingDuration)
		{
			changeState(ETransitionState::Idle);
			m_StateTimer = 0.0f;
		}

		break;
	}
}

void ULevelTransitionSubsystem::Deinitialize()
{
	m_ActorsToPoll.Empty();

	if (ULevelStreamingSubSystem* levelStreamingSystem = tryGetLevelStreamingSubSystem())
	{
		levelStreamingSystem->OnLoadLevelComplete.RemoveAll(this);
		levelStreamingSystem->OnUnloadLevelComplete.RemoveAll(this);
	}
}

bool ULevelTransitionSubsystem::RegisterActor(ALevelTransitionActorBase* ActorToRegister)
{
	if (ActorToRegister)
	{
		m_ActorsToPoll.AddUnique(ActorToRegister);
		return true;
	}

	return false;
}

bool ULevelTransitionSubsystem::UnregisterActor(ALevelTransitionActorBase* ActorToRemove)
{
	if (ActorToRemove && m_ActorsToPoll.Remove(ActorToRemove))
	{
		return true;
	}

	return false;
}

bool ULevelTransitionSubsystem::TransitionToLevel(const FString& levelPackageName, FTransitionSettings settings)
{
	if (m_State != ETransitionState::Idle)
	{
		return false;
	}

	ULevelStreamingSubSystem* levelStreamingSystem = GetWorld()->GetSubsystem<ULevelStreamingSubSystem>();
	if (!levelStreamingSystem->IsValidLowLevel())
	{
		return false;
	}

	TransitionSettings = settings;
	m_LevelPackageNameToBeLoaded = levelPackageName;
	changeState(ETransitionState::StartingTransition);
	m_StateTimer = 0.0f;
	return true;
}

void ULevelTransitionSubsystem::changeState(ETransitionState newState)
{
	ETransitionState prevState = m_State;
	m_State = newState;
	if (newState != prevState)
	{
		for (ALevelTransitionActorBase* actor : m_ActorsToPoll)
		{
			if (!actor)
			{
				continue;
			}

			actor->OnTransitionStateChanged(prevState, newState);
		}
	}
}

void ULevelTransitionSubsystem::handleOnUnloadLevelComplete()
{
	// Unloading has completed, start loading state
	if (ULevelStreamingSubSystem* levelStreamingSystem = tryGetLevelStreamingSubSystem())
	{
		levelStreamingSystem->OnUnloadLevelComplete.RemoveDynamic(this, &ULevelTransitionSubsystem::handleOnUnloadLevelComplete);
		levelStreamingSystem->OnLoadLevelComplete.AddUniqueDynamic(this, &ULevelTransitionSubsystem::handleOnLoadLevelComplete);
		levelStreamingSystem->TryLoadLevelAsRoot(m_LevelPackageNameToBeLoaded);
		changeState(ETransitionState::Loading);
	}
}

void ULevelTransitionSubsystem::handleOnLoadLevelComplete(FLoadLevelCompleteEventData eventData)
{
	// Loading has completed, start waiting state
	if (ULevelStreamingSubSystem* levelStreamingSystem = tryGetLevelStreamingSubSystem())
	{
		levelStreamingSystem->OnLoadLevelComplete.RemoveDynamic(this, &ULevelTransitionSubsystem::handleOnLoadLevelComplete);
		changeState(ETransitionState::Waiting);
		m_StateTimer = 0.0f;
	}
}
