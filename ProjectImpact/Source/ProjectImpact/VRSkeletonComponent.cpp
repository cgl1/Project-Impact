// Fill out your copyright notice in the Description page of Project Settings.


#include "VRSkeletonComponent.h"

#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

// Sets default values for this component's properties
UVRSkeletonComponent::UVRSkeletonComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVRSkeletonComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


void UVRSkeletonComponent::SetMotionControllers(UMotionControllerComponent* leftController, UMotionControllerComponent* rightController)
{
	m_LeftMotionController = leftController;
	m_RightMotionController = rightController;
}

FVector UVRSkeletonComponent::GetHeadWorldLocation()
{
	FVector position;
	FRotator orientation;
	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(orientation, position);
	return position;
}

FVector UVRSkeletonComponent::GetHeadForwardVector()
{
	FVector position;
	FRotator orientation;
	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(orientation, position);
	return FVector{ 1, 0, 0 };//UKismetMathLibrary::GetForwardVector(orientation);
}

FVector UVRSkeletonComponent::GetNeckWorldLocation()
{
	FVector headPos = GetHeadWorldLocation();
	FVector neckPos = headPos + FVector{ 0, 0, HeadToNeckOffsetZ };
	return neckPos;
}

FVector UVRSkeletonComponent::GetLeftHandLocation()
{
	if (!m_LeftMotionController)
	{
		return FVector{};
	}

	return m_LeftMotionController->GetComponentLocation();
}

FVector UVRSkeletonComponent::GetRightHandLocation()
{
	if (!m_RightMotionController)
	{
		return FVector{};
	}

	return m_RightMotionController->GetComponentLocation();
}

FVector UVRSkeletonComponent::GetLeftShoulderLocation()
{
	FVector leftHand = GetLeftHandLocation();
	FVector headUp = FVector::UpVector;
	FVector headForward = GetHeadForwardVector();
	FVector neckToRightShoulderVector = FVector::CrossProduct(headForward, headUp).GetSafeNormal();
	
	float neckToLeftShoulderOffset = ShoulderWidth * 0.5f;
	FVector leftShoulderLocation = GetNeckWorldLocation() + neckToRightShoulderVector * neckToLeftShoulderOffset;
	
	return leftShoulderLocation;
}

FVector UVRSkeletonComponent::GetRightShoulderLocation()
{
	FVector leftHand = GetLeftHandLocation();
	FVector headUp = FVector::UpVector;
	FVector headForward = GetHeadForwardVector();
	FVector neckToRightShoulderVector = FVector::CrossProduct(headForward, headUp).GetSafeNormal();

	float neckToRightShoulderOffset = ShoulderWidth * -0.5f;
	FVector rightShoulderLocation = GetNeckWorldLocation() + neckToRightShoulderVector * neckToRightShoulderOffset;

	return rightShoulderLocation;
}

FVRSkeletonData UVRSkeletonComponent::GetSkeletonData()
{
	FVRSkeletonData data{};

	FVector headPosition;
	FRotator headOrientation;
	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(headOrientation, headPosition);
	FVector headForward = FVector{ 1, 0, 0 }; //UKismetMathLibrary::GetForwardVector(headOrientation);
	FVector headUp = FVector::UpVector;
	data.HeadPosition = headPosition;
	data.HeadForwardVector = headForward;
	data.HeadOrientation = headOrientation;
	data.HeadUpVector = headUp;
	data.NeckPosition = headPosition + FVector{ 0, 0, HeadToNeckOffsetZ };
	data.LeftHandPosition = GetLeftHandLocation();
	data.RightHandPosition = GetRightHandLocation();

	FVector neckToRightShoulderVector = FVector::CrossProduct(headForward, headUp).GetSafeNormal();
	float neckToLeftShoulderOffset = ShoulderWidth * 0.5f;
	float neckToRightShoulderOffset = ShoulderWidth * -0.5f;
	data.LeftShoulderPosition = data.NeckPosition + neckToRightShoulderVector * neckToLeftShoulderOffset;
	data.RightShoulderPosition = data.NeckPosition + neckToRightShoulderVector * neckToRightShoulderOffset;

	return data;
}


FPoseCheckResult UVRSkeletonComponent::CheckPose(FPoseCheckParameters parameters)
{
	FPoseCheckResult result{};

	FVRSkeletonData skeletonData = GetSkeletonData();

	// Calculate angle difference between the arm and the target direction value
	
	// Body forward is basically head forward but ignoring vertical changes
	FVector bodyForwardVector = skeletonData.HeadForwardVector;
	bodyForwardVector.Z = 0;
	FRotator bodyForwardOrientation = bodyForwardVector.ToOrientationRotator();

	FVector targetLeftArmDirectionInSkeletonSpace = bodyForwardOrientation.RotateVector(parameters.LeftArmDirection);
	FVector targetRightArmDirectionInSkeletonSpace = bodyForwardOrientation.RotateVector(parameters.RightArmDirection);

	FVector leftArmVector = skeletonData.LeftHandPosition - skeletonData.LeftShoulderPosition;
	FVector rightArmVector = skeletonData.RightHandPosition - skeletonData.RightShoulderPosition;

	float leftArmVectorDot = FVector::DotProduct(leftArmVector.GetSafeNormal(), targetLeftArmDirectionInSkeletonSpace.GetSafeNormal());
	result.LeftArmAngleDiffInDegree = UKismetMathLibrary::DegAcos(leftArmVectorDot);

	float rightArmVectorDot = FVector::DotProduct(rightArmVector.GetSafeNormal(), targetRightArmDirectionInSkeletonSpace.GetSafeNormal());
	result.RightArmAngleDiffInDegree = UKismetMathLibrary::DegAcos(rightArmVectorDot);

	// Calculate arm length
	result.LeftArmLength = leftArmVector.Size();
	result.RightArmLength = rightArmVector.Size();

	result.LeftSuccess = result.LeftArmAngleDiffInDegree <= parameters.ArmAngleToleranceInDegree && FMath::Abs(result.LeftArmLength - parameters.LeftArmLength) <= parameters.ArmLengthTolerance;
	result.RightSuccess = result.RightArmAngleDiffInDegree <= parameters.ArmAngleToleranceInDegree && FMath::Abs(result.RightArmLength - parameters.RightArmLength) <= parameters.ArmLengthTolerance;
	return result;
}


FPoseHandLocations UVRSkeletonComponent::GetPoseHandLocations(FPoseCheckParameters parameters)
{
	FVRSkeletonData skeletonData = GetSkeletonData();

	// Body forward is basically head forward but ignoring vertical changes
	FVector bodyForwardVector = skeletonData.HeadForwardVector;
	bodyForwardVector.Z = 0;
	FRotator bodyForwardOrientation = bodyForwardVector.ToOrientationRotator();

	// Hands location
	FVector leftHandLocation = skeletonData.LeftShoulderPosition + bodyForwardOrientation.RotateVector(parameters.LeftArmDirection.GetSafeNormal()) * parameters.LeftArmLength;
	FVector rightHandLocation = skeletonData.RightShoulderPosition + bodyForwardOrientation.RotateVector(parameters.RightArmDirection.GetSafeNormal()) * parameters.RightArmLength;

	FPoseHandLocations locations{};
	locations.LeftHand = leftHandLocation;
	locations.RightHand = rightHandLocation;

	return locations;
}

void UVRSkeletonComponent::DrawDebugSkeleton(FVector skeletonOffset)
{
	FVRSkeletonData skeletonData = GetSkeletonData();

	float lifeTime = 0.0f;
	float thickness = 0.5f;
	float sphereRadius = 5.0f;

	// Head, Neck
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.HeadPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.NeckPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.HeadPosition, skeletonOffset + skeletonData.NeckPosition, FColor::Blue, false, lifeTime, '\000', thickness);

	// Neck, Ground
	FVector groundPosition = skeletonData.HeadPosition; 
	groundPosition.Z = 0;
	DrawDebugSphere(GetWorld(), skeletonOffset + groundPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.NeckPosition, skeletonOffset + groundPosition, FColor::Blue, false, lifeTime, '\000', thickness);

	// Left Shoulder, Hand
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.NeckPosition, skeletonOffset + skeletonData.LeftShoulderPosition, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.LeftHandPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.LeftShoulderPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.LeftHandPosition, skeletonOffset + skeletonData.LeftShoulderPosition, FColor::Blue, false, lifeTime, '\000', thickness);

	// Right Shoulder, Hand
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.NeckPosition, skeletonOffset + skeletonData.RightShoulderPosition, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.RightHandPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugSphere(GetWorld(), skeletonOffset + skeletonData.RightShoulderPosition, sphereRadius, 12, FColor::Blue, false, lifeTime, '\000', thickness);
	DrawDebugLine(GetWorld(), skeletonOffset + skeletonData.RightHandPosition, skeletonOffset + skeletonData.RightShoulderPosition, FColor::Blue, false, lifeTime, '\000', thickness);

	FString leftArmLength = FString::SanitizeFloat((skeletonData.LeftShoulderPosition - skeletonData.LeftHandPosition).Size());
	DrawDebugString(GetWorld(), skeletonOffset + skeletonData.LeftShoulderPosition, *leftArmLength, GetOwner(), FColor::Blue, lifeTime);

	FString rightArmLength = FString::SanitizeFloat((skeletonData.RightShoulderPosition - skeletonData.RightHandPosition).Size());
	DrawDebugString(GetWorld(), skeletonOffset + skeletonData.RightShoulderPosition, *rightArmLength, GetOwner(), FColor::Blue, lifeTime);
}

void UVRSkeletonComponent::DrawDebugPose(FPoseCheckParameters parameters, FVector skeletonOffset)
{
	FVRSkeletonData skeletonData = GetSkeletonData();

	float lifeTime = 0.0f;
	float thickness = 0.5f;
	float sphereRadius = 5.0f;

	FPoseCheckResult result = CheckPose(parameters);

	// Body forward is basically head forward but ignoring vertical changes
	FVector bodyForwardVector = skeletonData.HeadForwardVector;
	bodyForwardVector.Z = 0;
	FRotator bodyForwardOrientation = bodyForwardVector.ToOrientationRotator();

	// Left arm
	FColor color = result.LeftSuccess? FColor::Yellow : FColor::Red;
	DrawDebugLine
	(
		GetWorld(), 
		skeletonOffset + skeletonData.LeftShoulderPosition, 
		skeletonOffset + skeletonData.LeftShoulderPosition + bodyForwardOrientation.RotateVector(parameters.LeftArmDirection.GetSafeNormal()) * parameters.LeftArmLength,
		color, 
		false, 
		lifeTime, 
		'\000', 
		thickness
	);
	
	// Right arm
	color = result.RightSuccess? FColor::Yellow : FColor::Red;
	DrawDebugLine
	(
		GetWorld(), 
		skeletonOffset + skeletonData.RightShoulderPosition, 
		skeletonOffset + skeletonData.RightShoulderPosition + bodyForwardOrientation.RotateVector(parameters.RightArmDirection.GetSafeNormal()) * parameters.RightArmLength,
		color, 
		false, 
		lifeTime, 
		'\000', 
		thickness
	);
}
