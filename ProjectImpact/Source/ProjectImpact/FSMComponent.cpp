// Fill out your copyright notice in the Description page of Project Settings.


#include "FSMComponent.h"

// Sets default values for this component's properties
UFSMComponent::UFSMComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFSMComponent::BeginPlay()
{
	Super::BeginPlay();
	SetComponentTickEnabled(false);
}


// Called every frame
void UFSMComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	OnTickState.Broadcast(m_CurrState, DeltaTime);
}


bool UFSMComponent::InitFSM(const int32 numberOfStates, const uint8 initialState)
{
	if (numberOfStates < 0 || numberOfStates > 255)
	{
		if (bPrintErrorsToLog)
			UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": invalid numberOfStates!"));

		return false;
	}

	this->m_NumOfStates = numberOfStates;

	if (!IsValidState(initialState))
	{
		if (bPrintErrorsToLog)
			UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": invalid InitialState!"));

		return false;
	}

	SetState(initialState);
	m_IsInitialized = true;
	SetComponentTickEnabled(true);

	return true;
}

bool UFSMComponent::SetState(const uint8 state)
{
	if (!IsValidState(state))
	{
		return false;
	}

	if (m_IsInitialized) 
	{
		OnExitState.Broadcast(m_CurrState, state);
	}

	uint8 prevState = m_CurrState;
	m_CurrState = state;
	m_CurrStateBeginTime = GetOwner()->GetWorld()->GetTimeSeconds();	// save game time when current state starts
	OnEnterState.Broadcast(prevState, m_CurrState);
	return true;
}

bool UFSMComponent::IsValidState(const uint8 state) const
{
	if (m_NumOfStates > 0) {
		return (state >= 0 && state < this->m_NumOfStates);
	}

	return false;
}

uint8 UFSMComponent::GetCurrentState() const
{
	return m_CurrState;
}

float UFSMComponent::GetTime() const
{
	return (GetOwner()->GetWorld()->GetTimeSeconds() - m_CurrStateBeginTime);
}