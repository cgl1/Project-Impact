// Fill out your copyright notice in the Description page of Project Settings.


#include "IncantationCasterComponent.h"

#include "IncantationData.h"
#include "ManifestationActorBase.h"
#include "ManifestationHolderSubsystem.h"
#include "ManifestationHolderComponent.h"

// Sets default values for this component's properties
UIncantationCasterComponent::UIncantationCasterComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UIncantationCasterComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UIncantationCasterComponent::SetExerciseEvaluator(UExerciseEvaluatorComponent* evaluator)
{
	if (m_ExerciseEvaluator)
	{
		// Unregister from previous evaluator
		m_ExerciseEvaluator->OnExerciseEnded.RemoveDynamic(this, &UIncantationCasterComponent::handleOnExerciseEnded);
	}

	m_ExerciseEvaluator = evaluator;
	m_ExerciseEvaluator->OnExerciseEnded.AddDynamic(this, &UIncantationCasterComponent::handleOnExerciseEnded);
}

bool UIncantationCasterComponent::StartIncantation(UIncantationData* incantation)
{
	if (!m_ExerciseEvaluator)
	{
		UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": You need an exercise evaluator to start an incantation! Call SetExerciseEvaluator to set one first."));
		return false;
	}

	if (m_CurrentIncantation)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": There is already an incantation. End the previous one before starting a new incantation"));
		return false;
	}

	m_CurrentIncantation = incantation;
	m_CurrentIncantationExerciseIndex = 0;

	if (bSpawnManifestation)
	{
		if (!m_CurrentIncantation->ManifestationType)
		{
			UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": bSpawnManifestation is set to true but there is no ManifestationType assigned in %s."), *m_CurrentIncantation->GetName());
		}
		else
		{
			FActorSpawnParameters spawnParameters{};
			spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			spawnParameters.Owner = GetOwner();
			FVector location{};
			FRotator rotation{};

			UManifestationHolderSubsystem* manifestationHolderSystem = GetWorld()->GetSubsystem<UManifestationHolderSubsystem>();
			UManifestationHolderComponent* holder = nullptr;
			if (manifestationHolderSystem)
			{
				holder = manifestationHolderSystem->GetManifestationHolderOfType(m_CurrentIncantation->ManifestationType);
				if (holder)
				{
					// If there is a holder of the manifestation type, use it as spawn location
					spawnParameters.Owner = holder->GetOwner();
					location = holder->GetComponentLocation();
					rotation = holder->GetComponentRotation();
					
					m_CurrentManifestation = GetWorld()->SpawnActor<AManifestationActorBase>(m_CurrentIncantation->ManifestationType, location, rotation, spawnParameters);
					m_CurrentManifestation->OnGenerated();
					
					holder->OnManifestationSpawnedAt(m_CurrentManifestation);
				}
				else
				{
					GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, "Cannot find a corresponding manifestation holder. Manifestation is not spawned");
				}
			}
		}
	}

	OnIncantationStarted.Broadcast(m_CurrentIncantation);
	m_ExerciseEvaluator->StartExercise(m_CurrentIncantation->ExerciseList[m_CurrentIncantationExerciseIndex].Exercise);

	return true;
}

void UIncantationCasterComponent::TerminateIncantation()
{
	if (!m_CurrentIncantation)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": There is no incantation assigned, no need to terminate it!"));
		return;
	}

	onEndIncantation(EIncantationEndType::Termination);
}

void UIncantationCasterComponent::handleOnExerciseEnded(FOnExerciseEndedEventData eventData)
{
	if (!m_CurrentIncantation)
	{
		UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": There is no incantation assigned, the exercise might not be performed by the incantation caster."));
		return;
	}

	if (eventData.Type == EExerciseEndType::Termination)
	{
		// If the exercise is terminated, no need to do further action.
		return;
	}

	UExerciseData* exercise = eventData.Exercise;
	int numberOfExercisePerformed = m_CurrentIncantationExerciseIndex + 1;
	int numberOfExerciseNeeded = m_CurrentIncantation->ExerciseList.Num();

	if (bSpawnManifestation && m_CurrentManifestation)
	{
		// Apply completed exercise to the manifestation if there is one
		FOnExercisePerformedParameters performedParameters;
		performedParameters.Exercise = exercise;
		performedParameters.NumberOfExercisePerformed = numberOfExercisePerformed;
		performedParameters.NumberOfExerciseNeeded = numberOfExerciseNeeded;

		m_CurrentManifestation->OnExercisePerformed(performedParameters);
	}

	if (numberOfExercisePerformed < numberOfExerciseNeeded)
	{
		// Start the next incantation exercise
		m_CurrentIncantationExerciseIndex++;
		m_ExerciseEvaluator->StartExercise(m_CurrentIncantation->ExerciseList[m_CurrentIncantationExerciseIndex].Exercise);
	}
	else
	{
		// All the exercises needed for the incantation have been completed
		onEndIncantation(EIncantationEndType::Completion);
	}
}

void UIncantationCasterComponent::onEndIncantation(EIncantationEndType endType)
{
	if (bSpawnManifestation && m_CurrentManifestation)
	{
		if (endType == EIncantationEndType::Completion)
		{
			m_CurrentManifestation->OnCast();
		}
		else
		{
			m_CurrentManifestation->OnTerminated();
		}
	}

	if (endType == EIncantationEndType::Termination)
	{
		m_ExerciseEvaluator->ForceEndExercise();
	}

	UIncantationData* incantation = m_CurrentIncantation;

	m_CurrentIncantation = nullptr;
	m_CurrentManifestation = nullptr;

	FOnIncantationEndedEventData eventData{};
	eventData.Incantation = incantation;
	eventData.Type = endType;
	OnIncantationEnded.Broadcast(eventData);
}

