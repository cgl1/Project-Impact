// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VRSkeletonCalibratorComponent.generated.h"

class UVRSkeletonComponent;

UENUM(BlueprintType)
enum class ECalibrationState : uint8
{
	Idle,
	WaitingForTPose,
	WaitingForHandsDownPose,
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCalibrationEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCalibrationStateChangedEvent, ECalibrationState, prevState, ECalibrationState, nextState);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UVRSkeletonCalibratorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FCalibrationEvent OnCalibrationStarted;
	
	UPROPERTY(BlueprintAssignable)
	FCalibrationEvent OnCalibrationEnded;

	UPROPERTY(BlueprintAssignable)
	FCalibrationStateChangedEvent OnStateChanged;

private:
	UVRSkeletonComponent* m_Skeleton = nullptr;
	ECalibrationState m_State = ECalibrationState::Idle;

public:	
	// Sets default values for this component's properties
	UVRSkeletonCalibratorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ECalibrationState GetState() const { return m_State; }

	UFUNCTION(BlueprintCallable)
	void StartCalibration(UVRSkeletonComponent* skeleton);

	UFUNCTION(BlueprintCallable)
	void ProceedToNextState();

private:	
	UFUNCTION(BlueprintCallable)
	void confirmTPose();
	
	UFUNCTION(BlueprintCallable)
	void confirmHandsDownPose();
};
