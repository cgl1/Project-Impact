// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ExerciseEvaluatorComponent.h"
#include "IncantationCasterComponent.generated.h"

class UIncantationData;
class AManifestationActorBase;

UENUM(BlueprintType)
enum class EIncantationEndType : uint8
{
	Completion,
	Termination
};

USTRUCT(BlueprintType)
struct FOnIncantationEndedEventData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UIncantationData* Incantation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EIncantationEndType Type;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIncantationEvent, UIncantationData*, incantation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIncantationEndedEvent, FOnIncantationEndedEventData, incantationEndedEventData);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UIncantationCasterComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bSpawnManifestation = true;
	
	UPROPERTY(BlueprintAssignable)
	FIncantationEvent OnIncantationStarted;

	UPROPERTY(BlueprintAssignable)
	FIncantationEndedEvent OnIncantationEnded;

private:
	UExerciseEvaluatorComponent* m_ExerciseEvaluator = nullptr;

	UIncantationData* m_CurrentIncantation = nullptr;
	AManifestationActorBase* m_CurrentManifestation = nullptr;
	int m_CurrentIncantationExerciseIndex = 0;

public:	
	// Sets default values for this component's properties
	UIncantationCasterComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UIncantationData* GetCurrentIncantation() const { return m_CurrentIncantation; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	AManifestationActorBase* GetCurrentManifestation() const { return m_CurrentManifestation; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentIncantationExerciseIndex() const { return m_CurrentIncantationExerciseIndex; }

	UFUNCTION(BlueprintCallable)
	void SetExerciseEvaluator(UExerciseEvaluatorComponent* evaluator);
	
	UFUNCTION(BlueprintCallable)
	bool StartIncantation(UIncantationData* incantation);

	UFUNCTION(BlueprintCallable)
	void TerminateIncantation();

private:
	UFUNCTION()
	void handleOnExerciseEnded(FOnExerciseEndedEventData eventData);

	void onEndIncantation(EIncantationEndType endType);
};
