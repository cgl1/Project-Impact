// Fill out your copyright notice in the Description page of Project Settings.


#include "HalfLifeLerpComponent.h"

// Sets default values for this component's properties
UHalfLifeLerpComponent::UHalfLifeLerpComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHalfLifeLerpComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHalfLifeLerpComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (!bIsLerping)
	{
		return;
	}

	FVector currentLocation = GetComponentLocation();
	if ((currentLocation - m_TargetLocation).SizeSquared() < 0.05f)
	{
		return;
	}

	FVector newLocation = FMath::Lerp(currentLocation, m_TargetLocation, 1.0f - FMath::Pow(0.5f, DeltaTime / LerpHalfLifeTime));
	SetWorldLocation(newLocation);
}

void UHalfLifeLerpComponent::SetTargetLocation(FVector location)
{
	m_TargetLocation = location;
}

