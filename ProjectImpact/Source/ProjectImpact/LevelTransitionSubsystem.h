// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "LevelStreamingSubSystem.h"
#include "LevelTransitionSubsystem.generated.h"

UENUM(BlueprintType)
enum class ETransitionState : uint8
{
	Idle,

	StartingTransition UMETA(ToolTip = "Play effect such as fading out the scene etc"),

	Unloading,
	Loading,
	Waiting  UMETA(ToolTip = "A optional waiting state between loading and ending"),

	EndingTransition UMETA(ToolTip = "Play effect such as fading in the scene etc")
};

USTRUCT(BlueprintType)
struct FTransitionSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StartingDuration = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float WaitingDuration = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float EndingDuration = 2;
};

class ALevelTransitionActorBase;
class ULevelStreamingSubSystem;

/**
 * 
 */
UCLASS()
class PROJECTIMPACT_API ULevelTransitionSubsystem : public UTickableWorldSubsystem
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTransitionSettings TransitionSettings{};

private:
	/** Actors to receive transition state events*/
	UPROPERTY()
	TArray<ALevelTransitionActorBase*> m_ActorsToPoll;

	ETransitionState m_State = ETransitionState::Idle;

	float m_StateTimer = 0.0f;

	ULevelStreamingSubSystem* m_LevelStreamingSubSystem = nullptr;

	FString m_LevelPackageNameToBeLoaded{};

protected:
	//~FTickableGameObject interface
	ETickableTickType GetTickableTickType() const override;

	void Tick(float DeltaTime) override;

	TStatId GetStatId() const override { RETURN_QUICK_DECLARE_CYCLE_STAT(ULevelTransitionSubsystem, STATGROUP_Tickables); }

	//~USubsystem interface
	void Deinitialize() override;

public:
	UFUNCTION(BlueprintCallable)
	ETransitionState GetTransitionState() const { return m_State; }

	/**
	* Registers the given actor
	*
	* @return True if actor registered
	*/
	bool RegisterActor(ALevelTransitionActorBase* ActorToRegister);

	/**
	* Remove this actor from the array of actors to poll with this subsystem
	*
	* @return	True if this actor was removed
	*/
	bool UnregisterActor(ALevelTransitionActorBase* ActorToRemove);

	/* Do transition to the level with the given package name as the root, and load its subLevels as well. */
	UFUNCTION(BlueprintCallable)
	bool TransitionToLevel(const FString& levelPackageName, FTransitionSettings settings);

private:
	ULevelStreamingSubSystem* tryGetLevelStreamingSubSystem() 
	{ 
		if (!m_LevelStreamingSubSystem)
		{
			m_LevelStreamingSubSystem = GetWorld()->GetSubsystem<ULevelStreamingSubSystem>();
		}
		return m_LevelStreamingSubSystem;
	}

	void changeState(ETransitionState newState);
	
	UFUNCTION()
	void handleOnUnloadLevelComplete();
	
	UFUNCTION()
	void handleOnLoadLevelComplete(FLoadLevelCompleteEventData eventData);
};
