// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ExerciseData.h"
#include "ExerciseCommandActorBase.h"
#include "ExerciseEvaluatorComponent.generated.h"

UENUM(BlueprintType)
enum class EStartExerciseResult : uint8
{
	Success UMETA(ToolTip = "Successfully start the given exercise"),
	SuccessAndCompletion UMETA(ToolTip = "Successfully start and complete the given exercise immediately because it has no command at all"),
	AlreadyRunningAnotherExercise UMETA(ToolTip = "There is another exercise in the process, call ForceEndExercise first if you want to terminate the previous exercise"),
	Failure
};

UENUM(BlueprintType)
enum class EExerciseEndType : uint8
{
	Completion,
	ForceCompletion,
	Termination
};

USTRUCT(BlueprintType)
struct FOnExerciseEndedEventData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UExerciseData* Exercise;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EExerciseEndType Type;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExerciseStartedEvent, UExerciseData*, exercise);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExerciseEndedEvent, FOnExerciseEndedEventData, exerciseEndedEventData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCommandStartEvent, const FExerciseCommand&, command);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCommandEvaluationResultEvent, FCommandEvaluationResult, result);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UExerciseEvaluatorComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	bool m_IsExercising = false;
	UExerciseData* m_CurrentExercise = nullptr;

	int m_CurrentExerciseCommandIndex = 0;
	TArray<AExerciseCommandActorBase*> m_CommandActors;

	FInputActionBinding* m_CurrentCommandInputActionBinding = nullptr;

	float m_TickTimer = 0.0f;

	/* Whether or not the owner of this component has InputComponent enabled to listen to input action. */
	bool m_HasInputComponent = false;
	UInputComponent* m_InputComponent = nullptr;

public:
	UPROPERTY(BlueprintAssignable)
	FExerciseStartedEvent OnExerciseStarted;
	
	UPROPERTY(BlueprintAssignable)
	FExerciseEndedEvent OnExerciseEnded;

	UPROPERTY(BlueprintAssignable)
	FCommandStartEvent OnCommandStarted;

	UPROPERTY(BlueprintAssignable)
	FCommandEvaluationResultEvent OnCommandEvaluated;

public:	
	// Sets default values for this component's properties
	UExerciseEvaluatorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UExerciseData* GetCurrentExercise() const { return m_CurrentExercise; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurrentCommandIndex() const { return m_CurrentExerciseCommandIndex; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AExerciseCommandActorBase* GetCurrentCommandActor() const { return m_CommandActors[m_CurrentExerciseCommandIndex]; }

	UFUNCTION(BlueprintCallable)
	EStartExerciseResult StartExercise(UExerciseData* exercise);

	UFUNCTION(BlueprintCallable)
	void ForceEndExercise();
	
	UFUNCTION(BlueprintCallable)
	bool ForceCompleteExercise();

private:
	void bindCommandEvaluationInput(FName inputActionName, EInputEvent inputEvent);
	void unbindCommandEvaluationInput();
	void handleOnCommandInput();

	void onEvaluateCommand(float tickDeltaTime);
	void startCommandOfIndex(int commandIndex);
	void onEndExercise(EExerciseEndType endType);
};
