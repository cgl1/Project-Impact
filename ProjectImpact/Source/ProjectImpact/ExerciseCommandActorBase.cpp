// Fill out your copyright notice in the Description page of Project Settings.


#include "ExerciseCommandActorBase.h"

// Sets default values
AExerciseCommandActorBase::AExerciseCommandActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AExerciseCommandActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExerciseCommandActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExerciseCommandActorBase::StartCommand_Implementation()
{
}

FCommandEvaluationResult AExerciseCommandActorBase::Evaluate_Implementation(FEvaluationContext context)
{
	return FCommandEvaluationResult{};
}

void AExerciseCommandActorBase::EndCommand_Implementation()
{
}

void AExerciseCommandActorBase::ForceEndCommand_Implementation()
{
}
