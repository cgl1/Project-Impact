// Fill out your copyright notice in the Description page of Project Settings.


#include "ManifestationActorBase.h"

// Sets default values
AManifestationActorBase::AManifestationActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AManifestationActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AManifestationActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AManifestationActorBase::OnGenerated_Implementation()
{
}

void AManifestationActorBase::OnExercisePerformed_Implementation(FOnExercisePerformedParameters eventData)
{
}

void AManifestationActorBase::OnCast_Implementation()
{
}

void AManifestationActorBase::OnTerminated_Implementation()
{
}

