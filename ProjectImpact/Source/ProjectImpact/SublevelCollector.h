// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SublevelCollector.generated.h"

UCLASS()
class PROJECTIMPACT_API ASublevelCollector : public AActor
{
	GENERATED_BODY()
	
public:
	/** Levels names affected by this level streaming volume. */
	UPROPERTY(Category=LevelStreaming, VisibleAnywhere, BlueprintReadOnly, meta=(DisplayName = "Sub-Levels"))
	TArray<FName> SubLevelNames;

private:

#if WITH_EDITOR
	/** Updates list of streaming levels that are referenced by this streaming volume */
	UFUNCTION(Category=LevelStreaming, BlueprintCallable, CallInEditor)
	void updateSubLevelsRefs();
#endif

};
