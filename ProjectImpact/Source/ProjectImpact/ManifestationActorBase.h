// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ManifestationActorBase.generated.h"

class UExerciseData;

USTRUCT(BlueprintType)
struct FOnExercisePerformedParameters
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UExerciseData* Exercise;

	/* The number of exercise performed, including the current one. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ToolTip = "The number of exercise performed, including the current one."))
	int NumberOfExercisePerformed = 0;

	/* The number of exercise needed to form complete the incantation. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int NumberOfExerciseNeeded = 0;
};

UCLASS()
class PROJECTIMPACT_API AManifestationActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AManifestationActorBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnGenerated();
	void OnGenerated_Implementation();
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnExercisePerformed(FOnExercisePerformedParameters eventData);
	void OnExercisePerformed_Implementation(FOnExercisePerformedParameters eventData);

	/* OnCast is called when the manifestation is fully completed (the caster has performed all the exercsies in the corresponding incantation) */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnCast();
	void OnCast_Implementation();

	/* OnTerminated is called when the manifestation is discarded (the caster has ended the corresponding incantation before it is completed) */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void OnTerminated();
	void OnTerminated_Implementation();
};
