// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LoadLevelOnBeginPlay.generated.h"

UCLASS()
class PROJECTIMPACT_API ALoadLevelOnBeginPlay : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category=LevelStreaming)
	TSoftObjectPtr<UWorld> LevelAsset;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
#if WITH_EDITOR
	UFUNCTION(CallInEditor, Category=LevelStreaming)
	void smartReferenceLevelPackageName();
#endif // WITH EDITOR
};
