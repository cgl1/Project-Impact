// Fill out your copyright notice in the Description page of Project Settings.


#include "ManifestationHolderComponent.h"
#include "ManifestationHolderSubsystem.h"

// Sets default values for this component's properties
UManifestationHolderComponent::UManifestationHolderComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UManifestationHolderComponent::BeginPlay()
{
	Super::BeginPlay();

	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}

	UManifestationHolderSubsystem* system = world->GetSubsystem<UManifestationHolderSubsystem>();
	if (!system)
	{
		return;
	}

	system->RegisterHolder(this);
}

void UManifestationHolderComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}

	UManifestationHolderSubsystem* system = world->GetSubsystem<UManifestationHolderSubsystem>();
	if (!system)
	{
		return;
	}

	system->UnregisterHolder(this);

	Super::EndPlay(EndPlayReason);
}

void UManifestationHolderComponent::OnManifestationSpawnedAt_Implementation(AManifestationActorBase* manifestation)
{
}

