// Fill out your copyright notice in the Description page of Project Settings.


#include "ManifestationHolderSubsystem.h"

#include "ManifestationHolderComponent.h"

bool UManifestationHolderSubsystem::RegisterHolder(UManifestationHolderComponent* holderToRegister)
{
	if (holderToRegister)
	{
		m_ManifestationHolders.AddUnique(holderToRegister);
		return true;
	}

	return false;
}

bool UManifestationHolderSubsystem::UnregisterHolder(UManifestationHolderComponent* holderToRemove)
{
	if (holderToRemove && m_ManifestationHolders.Remove(holderToRemove))
	{
		return true;
	}

	return false;
}

UManifestationHolderComponent* UManifestationHolderSubsystem::GetManifestationHolderOfType(TSubclassOf<AManifestationActorBase> manifestationType)
{
	for (UManifestationHolderComponent* holder : m_ManifestationHolders)
	{
		UClass* holderManifestationType = holder->ManifestationType.Get();
		if (manifestationType == holderManifestationType)
		{
			return holder;
		}
	}

	return nullptr;
}
