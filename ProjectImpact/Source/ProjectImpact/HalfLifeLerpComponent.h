// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "HalfLifeLerpComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UHalfLifeLerpComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bIsLerping = true;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float LerpHalfLifeTime = 0.5f;


private:
	FVector m_TargetLocation;

public:	
	// Sets default values for this component's properties
	UHalfLifeLerpComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable)
	void SetTargetLocation(FVector location);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FVector GetTargetLocation() const { return m_TargetLocation; }
};
