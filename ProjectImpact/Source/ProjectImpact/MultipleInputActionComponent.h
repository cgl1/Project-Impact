// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MultipleInputActionComponent.generated.h"

DECLARE_DELEGATE_OneParam(FMultipleInputDelegate, const int32);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMultipleInputEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSingleInputEvent, FName, inputActionName);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UMultipleInputActionComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray<FName> InputActionTests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool bHasHoldTimer = true;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = "bHasHoldTimer"))
	bool bRepeatHoldInputEvent = true;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = "bHasHoldTimer"))
	float HoldTime = 1.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = "bHasHoldTimer"))
	float HoldTimerDegradingSpeed = 1.0f;

	UPROPERTY(BlueprintAssignable)
	FMultipleInputEvent OnAllInputPressed;
	
	UPROPERTY(BlueprintAssignable, meta = (ToolTip = "When the state goes from 'all input pressed' to 'one of the inputs released'"))
	FMultipleInputEvent OnReleased;
	
	UPROPERTY(BlueprintAssignable, meta = (ToolTip = "Called every frame when all the inputs are pressed"))
	FMultipleInputEvent OnRepeatTick;
	
	UPROPERTY(BlueprintAssignable, meta = (ToolTip = "Called when all the inputs are held for more than HoldTime. This event can be called repeatedly in one pressing if bRepeatHoldInputEvent is true."))
	FMultipleInputEvent OnHoldTimeReached;

	UPROPERTY(BlueprintAssignable)
	FSingleInputEvent OnSingleInputPressed;
	
	UPROPERTY(BlueprintAssignable)
	FSingleInputEvent OnSingleInputReleased;

private:
	/* Whether or not the owner of this component has InputComponent enabled to listen to input action. */
	bool m_HasInputComponent = false;
	UInputComponent* m_InputComponent = nullptr;
	
	TArray<FName> m_InputActions;
	TArray<bool> m_InputBuffer;
	int m_PressedInputCounter = 0;
	
	float m_HolderTimer = 0;
	bool m_HasTriggeredHoldInputEvent = false;

	TArray<FInputActionBinding*> m_PressedInputAcitonBindings;
	TArray<FInputActionBinding*> m_ReleasedInputAcitonBindings;

public:	
	// Sets default values for this component's properties
	UMultipleInputActionComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetPressedInputCounter() const { return m_PressedInputCounter; }
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetHoldTimer() const { return m_HolderTimer; }

	UFUNCTION(BlueprintCallable, CallInEditor)
	void BindInputActions(TArray<FName> inputActionNames);
	
	UFUNCTION(BlueprintCallable, CallInEditor)
	void UnbindInputActions();

private:
	UFUNCTION()
	void handleOnInputPressed(const int32 inputIndex);
	
	UFUNCTION()
	void handleOnInputReleased(const int32 inputIndex);

	
};
