// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectImpactGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTIMPACT_API AProjectImpactGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
