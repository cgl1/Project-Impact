// Fill out your copyright notice in the Description page of Project Settings.


#include "SublevelCollector.h"
#include "Engine/World.h"
#include "Engine/LevelStreaming.h"

#if WITH_EDITOR

void ASublevelCollector::updateSubLevelsRefs()
{
	SubLevelNames.Reset();

	UWorld* OwningWorld = GetWorld();
	if (OwningWorld)
	{
		for (ULevelStreaming* LevelStreaming : OwningWorld->GetStreamingLevels())
		{
			SubLevelNames.Add(LevelStreaming->GetWorldAssetPackageFName());
		}
	}
}

#endif// WITH_EDITOR

