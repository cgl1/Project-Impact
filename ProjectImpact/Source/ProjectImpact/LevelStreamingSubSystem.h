// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "LevelStreamingSubSystem.generated.h"

USTRUCT(BlueprintType)
struct FLoadLevelAsRootResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

USTRUCT(BlueprintType)
struct FLoadLevelCompleteEventData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ULevelStreaming* LevelStreamingObject = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int NumberOfSubLevelsLoaded = 0;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FLoadLevelCompleteEvent, FLoadLevelCompleteEventData, loadLevelCompleteEventData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUnloadLevelCompleteEvent);

/**
 * 
 */
UCLASS()
class PROJECTIMPACT_API ULevelStreamingSubSystem : public UWorldSubsystem
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable)
	FLoadLevelCompleteEvent OnLoadLevelComplete;

	UPROPERTY(BlueprintAssignable)
	FUnloadLevelCompleteEvent OnUnloadLevelComplete;

private:
	ULevelStreaming* m_CurrentRootLevel = nullptr;

	/* A collection of levels that are loaded (including the root level). */
	TArray<ULevelStreaming*> m_CurrentStreamingLevels;

	int m_NumberOfLevelsToBeUnloaded = 0;
	int m_NumberOfSubLevelsToBeLoaded = 0;
	int m_NumberOfSubLevelsLoaded = 0;

public:
	/* Load the level with the given package name as the root, and load its subLevels as well. */
	UFUNCTION(BlueprintCallable)
	FLoadLevelAsRootResult TryLoadLevelAsRoot(const FString& levelPackageName);

	/* Unload all the levels that are currently loaded in the streaming levels list. */
	UFUNCTION(BlueprintCallable)
	bool TryUnloadAllLevels();

	/* Get the number of levels that are currently loaded in the streaming levels list. */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetNumberOfLoadedLevels() const { return m_CurrentStreamingLevels.Num(); }

private:
	UFUNCTION()
	void handleOnRootLevelShown();

	UFUNCTION()
	void handleOnSubLevelLoaded();

	void completeLoadingLevel();

	UFUNCTION()
	void handleOnLevelHidden();

	void completeUnloadingLevel();
};
