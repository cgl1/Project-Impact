// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ExerciseCommandActorBase.h"
#include "ExerciseData.generated.h"

/* When the evaluation happens. (i.e. OnTick, OnInputReleased etc) */
UENUM(BlueprintType)
enum class ECommandEvaluationType : uint8
{
	OnTick UMETA(ToolTip = "The evaluation happens on tick every frame."),
	OnInput
};

USTRUCT(BlueprintType)
struct FExerciseCommand
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString CommandName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString CommandDescription;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ECommandEvaluationType EvaluationType = ECommandEvaluationType::OnTick;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "EvaluationType == EEvaluationType::OnTick"))
	float TickTimeThreshold = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "EvaluationType != EEvaluationType::OnTick"))
	TEnumAsByte<EInputEvent> InputEventType = IE_Pressed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "EvaluationType != EEvaluationType::OnTick"))
	FName EvaluationInputActionName = TEXT("ConfirmExercisePose");

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AExerciseCommandActorBase> InGameInstanceType;
};

/**
 * 
 */
UCLASS(BlueprintType)
class PROJECTIMPACT_API UExerciseData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString ExerciseName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bCanForceComplete = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ToolTip = "When bRepeat is true, you can only terminate an exercise with ForceCompleteExercise() or ForceEndExercise()."))
	bool bRepeat = false;

	/** The sequence of commands to complete this exercise */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (TitleProperty = CommandName))
    TArray<FExerciseCommand> Commands;
};
