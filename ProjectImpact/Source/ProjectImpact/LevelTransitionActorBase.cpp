// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTransitionActorBase.h"

#include "LevelTransitionSubsystem.h"

// Sets default values
ALevelTransitionActorBase::ALevelTransitionActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALevelTransitionActorBase::BeginPlay()
{
	Super::BeginPlay();

	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}

	ULevelTransitionSubsystem* system = world->GetSubsystem<ULevelTransitionSubsystem>();
	if (!system)
	{
		return;
	}

	system->RegisterActor(this);
}

void ALevelTransitionActorBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	UWorld* world = GetWorld();
	if (!world)
	{
		return;
	}

	ULevelTransitionSubsystem* system = world->GetSubsystem<ULevelTransitionSubsystem>();
	if (!system)
	{
		return;
	}

	system->UnregisterActor(this);

	Super::EndPlay(EndPlayReason);
}

// Called every frame
void ALevelTransitionActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

