// Fill out your copyright notice in the Description page of Project Settings.


#include "MultipleInputActionComponent.h"

// Sets default values for this component's properties
UMultipleInputActionComponent::UMultipleInputActionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMultipleInputActionComponent::BeginPlay()
{
	Super::BeginPlay();

	m_InputComponent = GetOwner()->InputComponent;
	if (m_InputComponent)
	{
		m_HasInputComponent = true;
	}
	else
	{
		m_HasInputComponent = false;
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": The component does not belong to an actor with an InputComponent. The input will not be detected properly!"));
	}
}


// Called every frame
void UMultipleInputActionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool isAllInputPressed = m_PressedInputCounter == m_InputActions.Num();

	if (isAllInputPressed)
	{
		OnRepeatTick.Broadcast();
	}

	if (bHasHoldTimer)
	{
		if (isAllInputPressed)
		{
			if (!bRepeatHoldInputEvent && m_HasTriggeredHoldInputEvent)
			{
				return;
			}

			m_HolderTimer += DeltaTime;
			if (m_HolderTimer >= HoldTime)
			{
				m_HolderTimer -= HoldTime;
				m_HasTriggeredHoldInputEvent = true;
				OnHoldTimeReached.Broadcast();
			}
		}
		else
		{
			m_HolderTimer -= DeltaTime * HoldTimerDegradingSpeed;
			if (m_HolderTimer < 0)
			{
				m_HolderTimer = 0;
			}
		}
	}

}

void UMultipleInputActionComponent::BindInputActions(TArray<FName> inputActionNames)
{
	if (!m_HasInputComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": The component does not belong to an actor with an InputComponent. The input will not be detected properly!"));
		return;
	}

	m_InputActions.Append(inputActionNames);

	m_PressedInputAcitonBindings.Reset(m_InputActions.Num());
	m_ReleasedInputAcitonBindings.Reset(m_InputActions.Num());
	
	m_InputBuffer.Init(false, m_InputActions.Num());
	m_PressedInputCounter = 0;

	int index = 0;
	for (const FName& inputActionName : m_InputActions)
	{
		// Bind 'Pressed' event
		FInputActionBinding pressedActionBinding{ inputActionName, IE_Pressed };
		pressedActionBinding.bConsumeInput = true;
		pressedActionBinding.bExecuteWhenPaused = false;
		pressedActionBinding.ActionDelegate.BindDelegate<FMultipleInputDelegate>(this, &UMultipleInputActionComponent::handleOnInputPressed, index);
		FInputActionBinding* pressedInputActionBinding = &m_InputComponent->AddActionBinding(pressedActionBinding);

		m_PressedInputAcitonBindings.Add(pressedInputActionBinding);
		
		// Bind 'Released' event
		FInputActionBinding releasedActionBinding{ inputActionName, IE_Released };
		releasedActionBinding.bConsumeInput = true;
		releasedActionBinding.bExecuteWhenPaused = false;
		releasedActionBinding.ActionDelegate.BindDelegate<FMultipleInputDelegate>(this, &UMultipleInputActionComponent::handleOnInputReleased, index);
		FInputActionBinding* releasedInputActionBinding = &m_InputComponent->AddActionBinding(releasedActionBinding);

		m_ReleasedInputAcitonBindings.Add(releasedInputActionBinding);

		index++;
	}
}

void UMultipleInputActionComponent::UnbindInputActions()
{
	if (!m_HasInputComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": The component does not belong to an actor with an InputComponent. The input will not be detected properly!"));
		return;
	}

	for (int index = 0; index < m_PressedInputAcitonBindings.Num(); index++)
	{
		m_InputComponent->RemoveActionBinding(m_PressedInputAcitonBindings[index]->GetHandle());
		m_InputComponent->RemoveActionBinding(m_ReleasedInputAcitonBindings[index]->GetHandle());
	}

	m_PressedInputAcitonBindings.Reset();
	m_ReleasedInputAcitonBindings.Reset();

	m_InputActions.Reset();
}

void UMultipleInputActionComponent::handleOnInputPressed(const int32 inputIndex)
{
	if (inputIndex >= m_InputActions.Num())
	{
		return;
	}

	OnSingleInputPressed.Broadcast(m_InputActions[inputIndex]);

	if (m_InputBuffer[inputIndex])
	{
		return;
	}
	
	int prevPressedInputCounter = m_PressedInputCounter;
	m_InputBuffer[inputIndex] = true;
	m_PressedInputCounter++;
	
	if (m_PressedInputCounter == m_InputActions.Num())
	{
		OnAllInputPressed.Broadcast();
	}
}

void UMultipleInputActionComponent::handleOnInputReleased(const int32 inputIndex)
{
	if (inputIndex >= m_InputActions.Num())
	{
		return;
	}

	OnSingleInputReleased.Broadcast(m_InputActions[inputIndex]);

	if (!m_InputBuffer[inputIndex])
	{
		return;
	}

	int prevPressedInputCounter = m_PressedInputCounter;
	m_InputBuffer[inputIndex] = false;
	m_PressedInputCounter--;
	
	if (prevPressedInputCounter == m_InputActions.Num())
	{
		m_HasTriggeredHoldInputEvent = false;
		OnReleased.Broadcast();
	}
}
