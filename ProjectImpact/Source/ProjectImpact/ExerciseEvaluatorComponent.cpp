// Fill out your copyright notice in the Description page of Project Settings.


#include "ExerciseEvaluatorComponent.h"

// Sets default values for this component's properties
UExerciseEvaluatorComponent::UExerciseEvaluatorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UExerciseEvaluatorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	m_InputComponent = GetOwner()->InputComponent;
	if (m_InputComponent)
	{
		m_HasInputComponent = true;
	}
	else
	{
		m_HasInputComponent = false;
	}
}


// Called every frame
void UExerciseEvaluatorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...

	if (!m_IsExercising)
	{
		return;
	}

	if (m_CurrentExercise->Commands.Num() == 0)
	{
		return;
	}

	const FExerciseCommand& commandData = m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex];
	if (commandData.EvaluationType != ECommandEvaluationType::OnTick)
	{
		return;
	}

	// The command evaluation type is OnTick, evaluate the command!
	onEvaluateCommand(DeltaTime);
}

EStartExerciseResult UExerciseEvaluatorComponent::StartExercise(UExerciseData* exercise)
{
	if (m_IsExercising)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": An exercise was already in the process. End the previous exercise first before starting a new one!"));
		return EStartExerciseResult::AlreadyRunningAnotherExercise;
	}

	if (exercise == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": The given exercise is a nullptr"));
		return EStartExerciseResult::Failure;
	}

	if (exercise != m_CurrentExercise)
	{
		// The new exercise is different from the last one, destroy the previous command actors and populate new ones.

		// Clean up the previous command actors
		for (AExerciseCommandActorBase* commandActor : m_CommandActors)
		{
			commandActor->Destroy();
		}
		m_CommandActors.Reset();

		// Spawn new command actors
		FActorSpawnParameters spawnParameters;
		spawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		spawnParameters.Owner = this->GetOwner();
		for (const FExerciseCommand& commandData : exercise->Commands)
		{
			if (!commandData.InGameInstanceType)
			{
				FString message = FString::Printf(TEXT(__FUNCTION__": The command '%s' does not have a command actor class assigned."), *commandData.CommandName);
				UE_LOG(LogTemp, Error, TEXT("%s"), *message);
			}

			AExerciseCommandActorBase* commandActor = GetWorld()->SpawnActor<AExerciseCommandActorBase>(commandData.InGameInstanceType.Get(), spawnParameters);
			m_CommandActors.Add(commandActor);
		}
	}

	m_IsExercising = true;
	m_CurrentExercise = exercise;

	OnExerciseStarted.Broadcast(m_CurrentExercise);

	if (m_CurrentExercise->Commands.Num() > 0)
	{
		// Start the evaluation process on the first command.
		startCommandOfIndex(0);

		return EStartExerciseResult::Success;
	}
	else
	{
		// The exercise contains no command, complete the exercise immediately.
		onEndExercise(EExerciseEndType::Completion);
		return EStartExerciseResult::SuccessAndCompletion;
	}
}

void UExerciseEvaluatorComponent::ForceEndExercise()
{
	if (!m_IsExercising)
	{
		UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": The evaulator is not exercising, no need to force end!"));
		return;
	}

	const FExerciseCommand& commandData = m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex];
	if (commandData.EvaluationType == ECommandEvaluationType::OnInput)
	{
		unbindCommandEvaluationInput();
	}

	AExerciseCommandActorBase* commandActor = m_CommandActors[m_CurrentExerciseCommandIndex];
	commandActor->ForceEndCommand();

	onEndExercise(EExerciseEndType::Termination);
}

bool UExerciseEvaluatorComponent::ForceCompleteExercise()
{
	if (!m_IsExercising)
	{
		UE_LOG(LogTemp, Error, TEXT(__FUNCTION__": The evaulator is not exercising, no need to force complete!"));
		return false;
	}

	if (!m_CurrentExercise->bCanForceComplete)
	{
		UE_LOG(LogTemp, Display, TEXT(__FUNCTION__": The exercise cannot force complete!"));
		return false;
	}

	const FExerciseCommand& commandData = m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex];
	if (commandData.EvaluationType == ECommandEvaluationType::OnInput)
	{
		unbindCommandEvaluationInput();
	}

	AExerciseCommandActorBase* commandActor = m_CommandActors[m_CurrentExerciseCommandIndex];
	commandActor->ForceEndCommand();

	onEndExercise(EExerciseEndType::ForceCompletion);
	return true;
}

void UExerciseEvaluatorComponent::bindCommandEvaluationInput(FName inputActionName, EInputEvent inputEventType)
{
	if (!m_HasInputComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": %s doesn't have a valid InputComponent to listen to exercise command's input."), *GetOwner()->GetName());
		return;
	}

	// Bind input action
	FInputActionBinding actionBinding{ inputActionName, inputEventType };
	actionBinding.bConsumeInput = true;
	actionBinding.bExecuteWhenPaused = false;
	actionBinding.ActionDelegate.BindDelegate(this, &UExerciseEvaluatorComponent::handleOnCommandInput);

	m_CurrentCommandInputActionBinding = &m_InputComponent->AddActionBinding(actionBinding);
}

void UExerciseEvaluatorComponent::unbindCommandEvaluationInput()
{
	if (!m_HasInputComponent)
	{
		return;
	}

	if (!m_CurrentCommandInputActionBinding)
	{
		return;
	}

	// Unbind input action
	m_InputComponent->RemoveActionBindingForHandle(m_CurrentCommandInputActionBinding->GetHandle());
	m_CurrentCommandInputActionBinding = nullptr;
}

void UExerciseEvaluatorComponent::handleOnCommandInput()
{
	// The command evaluation input is triggered, evaluate the command!
	onEvaluateCommand(0.0f);
}

void UExerciseEvaluatorComponent::onEvaluateCommand(float tickDeltaTime)
{
	FEvaluationContext context{};
	context.OwnerActor = GetOwner();

	AExerciseCommandActorBase* commandActor = m_CommandActors[m_CurrentExerciseCommandIndex];
	if (!commandActor)
	{
		FString message = FString::Printf(TEXT(__FUNCTION__": The command '%s' does not have a command actor class assigned."), *m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex].CommandName);
		UE_LOG(LogTemp, Error, TEXT("%s"), *message);
		return;
	}

	FCommandEvaluationResult evaluationResult = commandActor->Evaluate(context);

	if (!evaluationResult.Success)
	{
		// Broadcast command evaluation result event
		OnCommandEvaluated.Broadcast(evaluationResult);
		return;
	}

	const FExerciseCommand& commandData = m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex];
	bool hasCompletedCurrentCommand = false;
	if (commandData.EvaluationType == ECommandEvaluationType::OnTick)
	{
		m_TickTimer += tickDeltaTime;

		if (m_TickTimer >= commandData.TickTimeThreshold)
		{
			// The accumulated time has reached the threshold.
			hasCompletedCurrentCommand = true;
		}
	}
	else if (commandData.EvaluationType == ECommandEvaluationType::OnInput)
	{
		hasCompletedCurrentCommand = true;
		unbindCommandEvaluationInput();
	}
	
	evaluationResult.Success = hasCompletedCurrentCommand;
	OnCommandEvaluated.Broadcast(evaluationResult);

	if (hasCompletedCurrentCommand)
	{
		// Do command transition or exercise completion.
		commandActor->EndCommand();

		int nextCommandIndex = m_CurrentExerciseCommandIndex + 1;
		if (nextCommandIndex >= m_CurrentExercise->Commands.Num())
		{
			// The current command is the last command.
			if (m_CurrentExercise->bRepeat)
			{
				// If the current exercise is set to repeat, restart from the first command.
				startCommandOfIndex(0);
			}
			else
			{
				// Complete exercise!
				onEndExercise(EExerciseEndType::Completion);
			}
		}
		else
		{
			// Go to the next command process.
			startCommandOfIndex(nextCommandIndex);
		}
	}
}

void UExerciseEvaluatorComponent::startCommandOfIndex(int commandIndex)
{
	m_CurrentExerciseCommandIndex = commandIndex;
	m_TickTimer = 0.0f;

	const FExerciseCommand& nextCommandData = m_CurrentExercise->Commands[m_CurrentExerciseCommandIndex];
	if (nextCommandData.EvaluationType == ECommandEvaluationType::OnInput)
	{
		bindCommandEvaluationInput(nextCommandData.EvaluationInputActionName, nextCommandData.InputEventType);
	}

	AExerciseCommandActorBase* commandActor = m_CommandActors[commandIndex];
	if (commandActor)
	{
		m_CommandActors[commandIndex]->StartCommand();
	}
	else
	{
		FString message = FString::Printf(TEXT(__FUNCTION__": The command '%s' does not have a command actor class assigned."), *nextCommandData.CommandName);
		UE_LOG(LogTemp, Error, TEXT("%s"), *message);
	}

	OnCommandStarted.Broadcast(nextCommandData);
}

void UExerciseEvaluatorComponent::onEndExercise(EExerciseEndType endType)
{
	m_IsExercising = false;

	FOnExerciseEndedEventData eventData{};
	eventData.Exercise = m_CurrentExercise;
	eventData.Type = endType;
	OnExerciseEnded.Broadcast(eventData);
}
