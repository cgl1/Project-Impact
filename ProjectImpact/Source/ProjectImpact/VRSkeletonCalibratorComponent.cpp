// Fill out your copyright notice in the Description page of Project Settings.


#include "VRSkeletonCalibratorComponent.h"

#include "VRSkeletonComponent.h"

// Sets default values for this component's properties
UVRSkeletonCalibratorComponent::UVRSkeletonCalibratorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVRSkeletonCalibratorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UVRSkeletonCalibratorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UVRSkeletonCalibratorComponent::StartCalibration(UVRSkeletonComponent* skeleton)
{
	m_Skeleton = skeleton;

	if (m_State != ECalibrationState::Idle)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": Calibration is already in process. Force abort and restart!"));
	}

	ECalibrationState previousState = m_State;
	m_State = ECalibrationState::WaitingForTPose;
	
	OnCalibrationStarted.Broadcast();
	OnStateChanged.Broadcast(previousState, m_State);
}

void UVRSkeletonCalibratorComponent::ProceedToNextState()
{
	if (m_State == ECalibrationState::Idle)
	{
		UE_LOG(LogTemp, Display, TEXT(__FUNCTION__": Calibration is not activated"));
		return;
	}

	if (!m_Skeleton)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": You need to assign a target skeleton first!"));
		return;
	}

	ECalibrationState previousState = m_State;
	if (m_State == ECalibrationState::WaitingForTPose)
	{
		confirmTPose();
		m_State = ECalibrationState::WaitingForHandsDownPose;
		OnStateChanged.Broadcast(previousState, m_State);
	}
	else if (m_State == ECalibrationState::WaitingForHandsDownPose)
	{
		confirmHandsDownPose();
		m_State = ECalibrationState::Idle;

		OnCalibrationEnded.Broadcast();
		OnStateChanged.Broadcast(previousState, m_State);
	}
}

void UVRSkeletonCalibratorComponent::confirmTPose()
{
	if (!m_Skeleton)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": You need to assign a target skeleton first!"));
		return;
	}

	FVRSkeletonData data = m_Skeleton->GetSkeletonData();
	FVector neckPosition = (data.LeftHandPosition + data.RightHandPosition) * 0.5f;
	FVector headPosition = data.HeadPosition;
	FVector headToNeck = neckPosition - headPosition;
	float headToNeckOffsetZ = headToNeck.Z;

	// Set head to neck offset value to the skeleton
	m_Skeleton->HeadToNeckOffsetZ = headToNeckOffsetZ;
}

void UVRSkeletonCalibratorComponent::confirmHandsDownPose()
{
	if (!m_Skeleton)
	{
		UE_LOG(LogTemp, Warning, TEXT(__FUNCTION__": You need to assign a target skeleton first!"));
		return;
	}

	// We assume the T pose (HeadToNeckOffsetZ) was confirmed already
	FVRSkeletonData data = m_Skeleton->GetSkeletonData();
	float shoulderWidth = (data.LeftHandPosition - data.RightHandPosition).Size();
	
	float leftArmLength = FMath::Abs(data.NeckPosition.Z - data.LeftHandPosition.Z);
	float rightArmLength = FMath::Abs(data.NeckPosition.Z - data.RightHandPosition.Z);
	float averageArmLength = (leftArmLength + rightArmLength) * 0.5f;
	
	// Set shoulder width & arm length value to the skeleton
	m_Skeleton->ShoulderWidth = shoulderWidth;
	m_Skeleton->ArmFullLength = averageArmLength;
}

