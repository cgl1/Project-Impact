// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "ManifestationActorBase.h"
#include "ManifestationHolderComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UManifestationHolderComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<AManifestationActorBase> ManifestationType;

public:	
	// Sets default values for this component's properties
	UManifestationHolderComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	UFUNCTION(BlueprintNativeEvent)
	void OnManifestationSpawnedAt(AManifestationActorBase* manifestation);
	void OnManifestationSpawnedAt_Implementation(AManifestationActorBase* manifestation);
		
};
