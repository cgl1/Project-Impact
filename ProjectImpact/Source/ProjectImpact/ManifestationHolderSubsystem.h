// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "ManifestationHolderSubsystem.generated.h"


//class UManifestationHolderComponent;

UCLASS()
class PROJECTIMPACT_API UManifestationHolderSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()
	
private:
	/** Actors to receive transition state events*/
	UPROPERTY()
	TArray<UManifestationHolderComponent*> m_ManifestationHolders;

public:

	/**
	* Registers the given holder
	*
	* @return True if holder registered
	*/
	bool RegisterHolder(UManifestationHolderComponent* holderToRegister);

	/**
	* Remove this holder from the array of holders to poll with this subsystem
	*
	* @return	True if this holder was removed
	*/
	bool UnregisterHolder(UManifestationHolderComponent* holderToRemove);

	UFUNCTION(BlueprintCallable)
	UManifestationHolderComponent* GetManifestationHolderOfType(TSubclassOf<AManifestationActorBase> manifestationType);
};
