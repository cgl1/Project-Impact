// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractorComponent.h"

#include "InteractableComponentBase.h"

// Sets default values for this component's properties
UInteractorComponent::UInteractorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInteractorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (bRefreshBestCandidateOnTick)
	{
		RefreshInteractableCandidate();
	}
}

FTriggerInteractResult UInteractorComponent::InteractWithBestCandidate()
{
	FTriggerInteractResult result{};
	if (m_BestCandidate)
	{
		result.Success = true;
		m_BestCandidate->Interact(this);
	}
	else
	{
		result.Success = false;
	}

	return result;
}

void UInteractorComponent::RefreshInteractableCandidate()
{
	UInteractableComponentBase* newBestCandidate = nullptr;
	float newBestCandidateDistanceSqr = 1000000000.0f;
	for (UInteractableComponentBase* candidate : m_InteractableCandidates)
	{
		if (!candidate->IsInteractable())
		{
			continue;
		}

		FVector candidateLocation = candidate->GetComponentLocation();
		FVector interactorLocation = this->GetComponentLocation();
		float distanceSqr = FVector::DistSquared(candidateLocation, interactorLocation);
		
		if (bHasCandidateDistanceThreshold && distanceSqr > CandidateDistanceThreshold * CandidateDistanceThreshold)
		{
			// The distance is over the threshold, skip it!
			continue;
		}

		if (distanceSqr < newBestCandidateDistanceSqr)
		{
			// The candidate is closer, set it as the best candidate temporarily.
			newBestCandidate = candidate;
		}
	}

	if (newBestCandidate != m_BestCandidate)
	{
		if (m_BestCandidate)
		{
			m_BestCandidate->OnDisqualifiedAsBestCandidateOf(this);
		}

		auto prevBestCandidate = m_BestCandidate;
		m_BestCandidate = newBestCandidate;

		if (newBestCandidate)
		{
			newBestCandidate->OnBecomeBestCandidateOf(this);
		}

		OnBestCandidateChanged.Broadcast(prevBestCandidate, newBestCandidate);
	}
}

FRegisterInteractableResult UInteractorComponent::RegisterInteractable(UInteractableComponentBase* interactable)
{
	int preexistingIndex = -1;
	if (m_InteractableCandidates.Find(interactable, preexistingIndex))
	{
		// TODO: return already exist info!
	}
	else
	{
		m_InteractableCandidates.Add(interactable);
		interactable->OnRegisteredTo(this);
	}
	return FRegisterInteractableResult();
}

FUnregisterInteractableResult UInteractorComponent::UnregisterInteractable(UInteractableComponentBase* interactable)
{
	m_InteractableCandidates.Remove(interactable);
	interactable->OnUnregisteredFrom(this);
	return FUnregisterInteractableResult();
}

void UInteractorComponent::handleOnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor)
{
	OtherActor->GetComponents<UInteractableComponentBase>(m_PreallocatedInteractablesArray);
	if (m_PreallocatedInteractablesArray.Num() > 0)
	{
		for (UInteractableComponentBase* interactable : m_PreallocatedInteractablesArray)
		{
			RegisterInteractable(interactable);
		}
	}

	RefreshInteractableCandidate();
}

void UInteractorComponent::handleOnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor)
{
	OtherActor->GetComponents<UInteractableComponentBase>(m_PreallocatedInteractablesArray);
	if (m_PreallocatedInteractablesArray.Num() > 0)
	{
		for (UInteractableComponentBase* interactable : m_PreallocatedInteractablesArray)
		{
			UnregisterInteractable(interactable);
		}
	}

	RefreshInteractableCandidate();
}

