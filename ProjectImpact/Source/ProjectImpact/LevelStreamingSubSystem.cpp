// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelStreamingSubSystem.h"

#include "Engine/LevelStreaming.h"
#include "Engine/LevelStreamingDynamic.h"
#include "Kismet/GameplayStatics.h"
#include "SublevelCollector.h"


FLoadLevelAsRootResult ULevelStreamingSubSystem::TryLoadLevelAsRoot(const FString& levelPackageName)
{
	FLoadLevelAsRootResult result{};

	// Load new level (and sublevels)
	bool loadLevelSuccess = false;
	auto newLevelStreaming = ULevelStreamingDynamic::LoadLevelInstance(this, levelPackageName, FVector::ZeroVector, FRotator::ZeroRotator, loadLevelSuccess, "");

	if (loadLevelSuccess)
	{
		// Register on shown event to check if there are sublevels when that happens.
		newLevelStreaming->OnLevelShown.AddDynamic(this, &ULevelStreamingSubSystem::handleOnRootLevelShown);

		m_CurrentRootLevel = newLevelStreaming;
		m_CurrentStreamingLevels.Add(newLevelStreaming);

		UE_LOG(LogTemp, Display, TEXT("Load as root level: %s"), *levelPackageName);
		result.Success = true;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to load as root level: %s"), *levelPackageName);
		result.Success = false;
	}

	return result;
}

void ULevelStreamingSubSystem::handleOnRootLevelShown()
{
	// Check if the root level has sublevels.
	// We do this in OnLevelShown event because that's when all the actors in the level are properly loaded.
	AActor* collectorActor = UGameplayStatics::GetActorOfClass(GetWorld(), ASublevelCollector::StaticClass());
	ASublevelCollector* collector = Cast<ASublevelCollector>(collectorActor);

	m_NumberOfSubLevelsToBeLoaded = 0;
	m_NumberOfSubLevelsLoaded = 0;

	if (collector->IsValidLowLevel())
	{
		for (const FName& subLevelFName : collector->SubLevelNames)
		{
			FString subLevelName = subLevelFName.ToString();
			bool loadLevelSuccess = false;
			ULevelStreaming* newLevelStreaming = ULevelStreamingDynamic::LoadLevelInstance(this, subLevelName, FVector::ZeroVector, FRotator::ZeroRotator, loadLevelSuccess, "");

			if (loadLevelSuccess)
			{
				newLevelStreaming->OnLevelLoaded.AddDynamic(this, &ULevelStreamingSubSystem::handleOnSubLevelLoaded);
				m_CurrentStreamingLevels.Add(newLevelStreaming);

				m_NumberOfSubLevelsToBeLoaded++;
			}
			else
			{
				m_NumberOfSubLevelsToBeLoaded--;
			}
		}
	}

	if (m_NumberOfSubLevelsToBeLoaded > 0)
	{
		// There are still sublevels to be loaded.
		return;
	}

	// No sublevels need to be waited for loading, complete process.
	completeLoadingLevel();
}

void ULevelStreamingSubSystem::handleOnSubLevelLoaded()
{
	m_NumberOfSubLevelsToBeLoaded--;
	m_NumberOfSubLevelsLoaded++;

	if (m_NumberOfSubLevelsToBeLoaded > 0)
	{
		// There are still sublevels to be loaded.
		return;
	}

	// All the sublevels have been loaded, complete process.
	completeLoadingLevel();
}

void ULevelStreamingSubSystem::completeLoadingLevel()
{
	// All the sublevels have been loaded, broadcast load level complete event.
	FLoadLevelCompleteEventData eventData{};
	eventData.LevelStreamingObject = m_CurrentRootLevel;
	eventData.NumberOfSubLevelsLoaded = m_NumberOfSubLevelsLoaded;
	OnLoadLevelComplete.Broadcast(eventData);
}

bool ULevelStreamingSubSystem::TryUnloadAllLevels()
{
	if (GetNumberOfLoadedLevels() > 0)
	{
		// Unload levels if there are levels loaded previously
		m_NumberOfLevelsToBeUnloaded = m_CurrentStreamingLevels.Num();
		for (ULevelStreaming* levelStreaming : m_CurrentStreamingLevels)
		{
			levelStreaming->OnLevelHidden.AddDynamic(this, &ULevelStreamingSubSystem::handleOnLevelHidden);
			levelStreaming->SetIsRequestingUnloadAndRemoval(true);
		}

		m_CurrentStreamingLevels.Reset();
	}
	else
	{
		// No levels are loaded, complete process.
		completeUnloadingLevel();
	}

	return true;
}

void ULevelStreamingSubSystem::handleOnLevelHidden()
{
	m_NumberOfLevelsToBeUnloaded--;

	if (m_NumberOfLevelsToBeUnloaded > 0)
	{
		// There are still levels to be undloaded.
		return;
	}

	completeUnloadingLevel();
}

void ULevelStreamingSubSystem::completeUnloadingLevel()
{
	OnUnloadLevelComplete.Broadcast();
}