// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "InteractableComponentBase.generated.h"

class UInteractorComponent;

USTRUCT(BlueprintType)
struct FInteractResult
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Success = true;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnInteractEvent, UInteractorComponent*, interactor, FInteractResult, result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractorEvent, UInteractorComponent*, interactor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FIsInteractableEvent, bool, isInteractable);

UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTIMPACT_API UInteractableComponentBase : public USceneComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnInteractEvent OnInteracted;

	UPROPERTY(BlueprintAssignable)
	FInteractorEvent OnBecomeBestCandidate;
	
	UPROPERTY(BlueprintAssignable)
	FInteractorEvent OnDisqualifiedAsBestCandidate;
	
	UPROPERTY(BlueprintAssignable)
	FIsInteractableEvent OnIsInteractableChanged;

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool m_IsInteractable = true;

private:
	TArray<UInteractorComponent*> m_RegisteredInteractors;
	TArray<UInteractorComponent*> m_BestCandidatedInteractors;

public:	
	// Sets default values for this component's properties
	UInteractableComponentBase();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetNumberOfBestCandidatedInteractors() const { return m_BestCandidatedInteractors.Num(); }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsInteractable() const { return m_IsInteractable; }

	UFUNCTION(BlueprintCallable)
	void SetIsInteractable(bool value);

	UFUNCTION(BlueprintCallable)
	FInteractResult Interact(UInteractorComponent* interactor);

	UFUNCTION(BlueprintCallable)
	void OnBecomeBestCandidateOf(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable)
	void OnDisqualifiedAsBestCandidateOf(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable)
	void OnRegisteredTo(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable)
	void OnUnregisteredFrom(UInteractorComponent* interactor);

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FInteractResult handleOnInteract(UInteractorComponent* interactor);
	FInteractResult handleOnInteract_Implementation(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void handleOnRegisteredTo(UInteractorComponent* interactor);
	void handleOnRegisteredTo_Implementation(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void handleOnUnregisteredFrom(UInteractorComponent* interactor);
	void handleOnUnregisteredFrom_Implementation(UInteractorComponent* interactor);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void handleOnBecomeBestCandidateOf(UInteractorComponent* interactor);
	void handleOnBecomeBestCandidateOf_Implementation(UInteractorComponent* interactor);
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void handleOnDisqualifiedAsBestCandidateOf(UInteractorComponent* interactor);
	void handleOnDisqualifiedAsBestCandidateOf_Implementation(UInteractorComponent* interactor);
};
